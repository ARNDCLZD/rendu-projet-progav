<%--
  Created by IntelliJ IDEA.
  User: Aimé
  Date: 10/11/2023
  Time: 08:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="styles/styles.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script>
        function redirectToLogin() {
            // Assuming the button is supposed to redirect to "/path/to/anotherPage.jsp"
            window.location.href = "index.jsp";
        }
    </script>
</head>
<body class="h-100 bg-light">
<div class="container" id="formTeacher">
    <div class="row bg-white rounded mt-2 w-50 p-2">
        <form action="login" method="post">
            <div class="col-12">
                <div class="row">
                    <h3 class="col-10">Créer un compte enseignant</h3>
                    <button class="btn btn-light col-2" type="button" onclick="redirectToLogin()">Retour</button>
                </div>
                <small class="text-muted mb-1">Vous représentez une école ? Contactez directement un administrateur.</small>
            </div>
            <hr class="hr" />
            <div class="row mb-1">
                <div class="col-6 form-group">
                    <label for="username" class="form-label">Nom d'utilisateur<small class="text-danger">*</small></label>
                    <input class="form-control" type="text" id="username" name="username" required>
                </div>
                <div class="col-6 form-group">
                    <label for="password" class="form-label">Mot de passe<small class="text-danger">*</small></label>
                    <input class="form-control" type="password" id="password" name="password" required>
                </div>
            </div>
            <div class="row mb-1">
                <div class="col-6 form-group">
                    <label for="firstName" class="form-label">Prénom<small class="text-danger">*</small></label>
                    <input class="form-control" type="text" id="firstName" name="firstName" required>
                </div>
                <div class="col-6 form-group">
                    <label for="name" class="form-label">Nom<small class="text-danger">*</small></label>
                    <input class="form-control" type="text" id="name" name="name" required>
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="experiences" class="form-label">Expériences</label>
                    <textarea class="form-control" rows="4" id="experiences" name="experiences"></textarea>
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="evaluations" class="form-label">Evaluations</label>
                    <textarea class="form-control" rows="4" id="evaluations" name="evaluations"></textarea>
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="skills" class="form-label">Compétences</label>
                    <textarea class="form-control" rows="4" id="skills" name="skills"></textarea>
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="studyLevel" class="form-label">Niveaux souhaités</label>
                    <textarea class="form-control" rows="4" id="studyLevel" name="studyLevel"></textarea>
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="website" class="form-label">Site web</label>
                    <input class="form-control" type="url" id="website" name="website">
                </div>
            </div>
            <div class="mb-1 row">
                <div class="form-group col-6">
                    <label for="phone" class="form-label">N° de téléphone<small class="text-danger">*</small></label>
                    <input class="form-control" type="number" id="phone" name="phone" required>
                </div>
                <div class="form-group col-6">
                    <label for="email" class="form-label">Email<small class="text-danger">*</small></label>
                    <input class="form-control" type="email" id="email" name="email" required>
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="availabilities" class="form-label">Disponibilités</label>
                    <input class="form-control" type="text" id="availabilities" name="availabilities">
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="contractType" class="form-label">Type de contrat</label>
                    <input class="form-control" type="text" id="contractType" name="contractType">
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="academicTitles" class="form-label">Titres académiques</label>
                    <input class="form-control" type="text" id="academicTitles" name="academicTitles">
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="jobReferences" class="form-label">Références professionnelles</label>
                    <input class="form-control" type="text" id="jobReferences" name="jobReferences">
                </div>
            </div>
            <div class="mb-1">
                <div class="form-group">
                    <label for="otherInformations" class="form-label">Autres informations</label>
                    <input class="form-control" type="text" id="otherInformations" name="otherInformations">
                </div>
            </div>
            <div class="mb-3">
                <div class="form-group">
                    <label for="references" class="form-label">Références</label>
                    <textarea class="form-control" rows="4" id="references" name="references"></textarea>
                </div>
            </div>
            <hr class="hr"/>
            <div class="d-flex justify-content-center">
                <button class="col-2 btn btn-primary mt-1" type="submit" name="action" value="register">Valider</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>