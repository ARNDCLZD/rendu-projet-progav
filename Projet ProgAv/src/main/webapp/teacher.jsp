<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Bomboclaat</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        function reloadTableOffers(listData, tableId, teacherLogin){
            $("#"+ tableId).empty();
            let firstLine = "<thead> <h1>Job Offers</h1><tr className='text-center'><th>Job Specification</th><th>Required Skills</th><th>Contract Length</th></tr></thead></tr>"
            $("#" +tableId).append(firstLine);
            listData.forEach((data, index)=>{
                let newLine = "<tr><td>" + data.jobSpecification + "</td><td>" + data.requiredSkills + "</td><td>" + data.contractLength + "</td><td><button type='button' onclick='apply(" + data.idOffer + ", \"" + teacherLogin + "\")' class='btn btn-primary'>Apply</button></td></tr>";
                $("#" +tableId).append(newLine);
            })
        }
        function reloadTableApplications(listData, tableId, teacherLogin){
            $("#"+ tableId).empty();
            let firstLine = "<thead> <h1>Applications</h1><tr className='text-center'><th>Job Specification</th><th>Required Skills</th><th>Contract Length</th></tr></thead></tr>"
            $("#" +tableId).append(firstLine);
            listData.forEach((data, index)=>{
                let newLine = "<tr><td>" + data.jobSpecification + "</td><td>" + data.requiredSkills + "</td><td>" + data.contractLength + "</td><td><button type='button' onclick='disapply(" + data.idOffer + ", \"" + teacherLogin + "\")' class='btn btn-primary'>Disapply</button></td></tr>";
                $("#" +tableId).append(newLine);
            })
        }
        function apply(idOffer, teacherLogin) {
            $.ajax({
                type: "GET",
                url: "/ProjetProgAv-1.0-SNAPSHOT/teacher",
                data: { action: "apply", idOffer: idOffer, teacherLogin: teacherLogin  },
                success: function (response) {
                    console.log(response);
                    if (response.result == "success"){
                        reloadTableOffers(response.jobOffersNotApplied, "offersTable", teacherLogin);
                        reloadTableApplications(response.jobApplicationsWaiting, "applicationsTable", teacherLogin);
                    }
                }
            });
        }
        function disapply(idOffer, teacherLogin) {
            $.ajax({
                type: "GET",
                url: "/ProjetProgAv-1.0-SNAPSHOT/teacher",
                data: { action: "disapply", idOffer: idOffer, teacherLogin: teacherLogin  },
                success: function (response) {
                    if (response.result == "success"){
                        reloadTableOffers(response.jobOffersNotApplied, "offersTable", teacherLogin);
                        reloadTableApplications(response.jobApplicationsWaiting, "applicationsTable", teacherLogin);
                    }
                }
            });
        }
        function disconnect() {
            $.ajax({
                type: "POST",
                url: "/ProjetProgAv-1.0-SNAPSHOT/teacher",
                data: { action: "disconnect"},
                success: function (response) {
                }
            });
        }

    </script>
</head>
    <body>
        <div class="container" >
            <c:choose>
                <c:when test="${teacher.registrationState=='accepted'}">
                    <div class="row bg-white rounded mt-2 w-50 p-2">
                        <form action="teacher" method="post">
                            <div class="col-12">
                                <div class="row">
                                    <h3 class="col-10">Bonjour Bonjour ${ teacher.firstName} ${ teacher.name}</h3>
                                    <a class="col-2 text-decoration-none" onclick="disconnect()">Se déconnecter</a>
                                </div>
                            </div>
                            <hr class="hr" />
                            <h3 class="mb-2">Informations personnelles</h3>
                            <div class="mb-1">
                                <div class="form-group">
                                    <label for="username" class="form-label">Nom d'utilisateur</label>
                                    <input class="form-control" type="text" id="username" name="username" value="${teacher.name}" readonly="readonly">
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6 form-group">
                                    <label for="firstName" class="form-label">Prénom</label>
                                    <input class="form-control" type="text" id="firstName" name="firstName" value="${teacher.firstName}" readonly="readonly">
                                </div>
                                <div class="col-6 form-group">
                                    <label for="name" class="form-label">Nom<small class="text-danger">*</small></label>
                                    <input class="form-control" type="text" id="name" name="name" value="${teacher.name}" readonly="readonly">
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group">
                                    <label for="experiences" class="form-label">Expériences</label>
                                    <textarea class="form-control" rows="4" id="experiences" name="experiences" readonly="readonly">${teacher.experiences}</textarea>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group">
                                    <label for="evaluations" class="form-label">Evaluations</label>
                                    <textarea class="form-control" rows="4" id="evaluations" name="evaluations" readonly="readonly">${teacher.evaluations}</textarea>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group">
                                    <label for="skills" class="form-label">Compétences</label>
                                    <textarea class="form-control" rows="4" id="skills" name="skills" readonly="readonly">${teacher.competences}</textarea>
                                </div>
                            </div>
                            <div class="mb-1 row">
                                <div class="form-group col-6">
                                    <label for="phone" class="form-label">N° de téléphone<small class="text-danger">*</small></label>
                                    <input class="form-control" type="number" id="phone" name="phone" value="${teacher.phone}" readonly="readonly">
                                </div>
                                <div class="form-group col-6">
                                    <label for="email" class="form-label">Email<small class="text-danger">*</small></label>
                                    <input class="form-control" type="email" id="email" name="email" value="${teacher.email}" readonly="readonly">
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group">
                                    <label for="academicTitles" class="form-label">Titres académiques</label>
                                    <input class="form-control" type="text" id="academicTitles" name="academicTitles" readonly="readonly" value="${teacher.academicTitles}">
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group">
                                    <label for="jobReferences" class="form-label">Références professionnelles</label>
                                    <input class="form-control" type="text" id="jobReferences" name="jobReferences" readonly="readonly" value="${teacher.jobReferences}">
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group">
                                    <label for="otherInformations" class="form-label">Autres informations</label>
                                    <input class="form-control" type="text" id="otherInformations" name="otherInformations" readonly="readonly" value="${teacher.otherInformations}">
                                </div>
                            </div>
                            <div class="mb-3">
                                <div class="form-group">
                                    <label for="references" class="form-label">Références</label>
                                    <textarea class="form-control" rows="4" id="references" name="references" readonly="readonly">${teacher.jobReferences}</textarea>
                                </div>
                            </div>
                            <hr class="hr"/>
                        </form>
                    </div>
                    <form method="get" action="/ProjetProgAv-1.0-SNAPSHOT/teacher">
                        <table id = "offersTable" class="table custab">
                            <thead>
                            <h1>Offres d'emplois</h1>
                            <tr class="text-center">
                                <th>Specifications</th>
                                <th>Compétences requises</th>
                                <th>Durée de contrat</th>
                            </tr>
                            </thead>
                            <c:forEach items="${jobOffersNotApplied}" var="offer">
                                <tr>
                                    <td>${offer.jobSpecification}</td>
                                    <td>${offer.requiredSkills}</td>
                                    <td>${offer.contractLength}</td>
                                    <td><button type=button onclick="apply(${offer.idOffer}, '${teacher.login}')" class="btn btn-primary">Apply</button></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </form>
                    <form method="get" action="/ProjetProgAv-1.0-SNAPSHOT/teacher">
                        <table id = "applicationsTable" class="table custab">
                            <thead>
                            <h1>Candidatures en cours</h1>
                            <tr class="text-center">
                                <th>Specifications</th>
                                <th>Compétences requises</th>
                                <th>Durée de contrat</th>
                            </tr>
                            </thead>
                            <c:forEach items="${jobOffersApplicationWaiting}" var="offer">
                                <tr>
                                    <td>${offer.jobSpecification}</td>
                                    <td>${offer.requiredSkills}</td>
                                    <td>${offer.contractLength}</td>
                                    <td><button  type=button onclick="disapply(${offer.idOffer}, '${teacher.login}')" class="btn btn-primary">Disapply</button></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </form>
                    <form method="get" action="/ProjetProgAv-1.0-SNAPSHOT/teacher">
                        <table class="table table-striped custab">
                            <thead>
                            <h1>Candidatures acceptées</h1>
                            <tr class="text-center">
                                <th>Specifications</th>
                                <th>Compétences requises</th>
                                <th>Durée de contrat</th>
                            </tr>
                            </thead>
                            <c:forEach items="${jobOffersApplicationAccepted}" var="offer">
                            <tr>
                                <td>${offer.jobSpecification}</td>
                                <td>${offer.requiredSkills}</td>
                                <td>${offer.contractLength}</td>
                                </c:forEach>
                        </table>
                    </form>
                    <form method="get" action="/ProjetProgAv-1.0-SNAPSHOT/teacher">
                        <table class="table table-striped custab">
                            <thead>
                            <h1>Candidatures refusées</h1>
                            <tr class="text-center">
                                <th>Specifications</th>
                                <th>Compétences requises</th>
                                <th>Durée de contrat</th>
                            </tr>
                            </thead>
                            <c:forEach items="${jobOffersApplicationRefused}" var="offer">
                            <tr>
                                <td>${offer.jobSpecification}</td>
                                <td>${offer.requiredSkills}</td>
                                <td>${offer.contractLength}</td>
                                </c:forEach>
                        </table>
                    </form>
                </c:when>
                <c:otherwise>
                    <h3 class="text-danger">Votre compte est en cours de validation, merci de contacter un administrateur.</h3>
                    <span>
                        <a href="index.jsp">Retour</a>
                    </span>
                </c:otherwise>
            </c:choose>
        </div>
    </body>
</html>
