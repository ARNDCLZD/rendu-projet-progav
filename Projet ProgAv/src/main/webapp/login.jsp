<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <title>Connexion</title>
    </head>
    <body class="bg-light">
        <div class="row h-100">
            <div class="d-flex justify-content-center my-auto">
                <div class="card" style="width: 25rem;">
                    <div class="card-body">
                        <h5 class="card-title">Connexion</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Vous serez redirigé au portail correspondant</h6>
                        <hr class="hr"/>
                        <div>
                            <form action="${pageContext.request.contextPath}/login" method="post">
                                <span>Vous êtes :</span>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="usertype" id="profradio" value="prof" checked="checked">
                                    <label class="form-check-label" for="profradio">Professeur</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="usertype" id="schoolradio" value="school">
                                    <label class="form-check-label" for="schoolradio">Recruteur</label>
                                </div>
                                <div class="form-group mb-2">
                                    <label for="username">
                                        Nom d'utilisateur
                                    </label>
                                    <input class="form-control" id="username" name="username" type="text">
                                </div>
                                <div class="form-group mb-2">
                                    <label for="password">
                                        Mot de passe
                                    </label>
                                    <input class="form-control" id="password" name="password" type="password" placeholder="*****">
                                </div>
                                <input class="btn btn-primary btn-sm" name="action" value="Login" type="submit">
                            </form>
                        </div>
                        <a href="inscription.jsp">
                            <small>Pas de compte ?</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
