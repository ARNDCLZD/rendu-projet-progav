<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/styles.css" rel="stylesheet">
  <title>Connexion</title>
</head>
<body class="bg-light">
<div class="row h-100">
  <div class="d-flex justify-content-center my-auto">
    <div class="card" style="width: 25rem;">
      <div class="card-body">
        <div class="row">
          <h5 class="col-8 card-title">Connexion</h5>
          <a class="col-4 text-decoration-none" href="inscription.jsp">
            <small>Pas de compte ?</small>
          </a>
        </div>
        <h6 class="card-subtitle mb-2 text-muted">Vous serez redirigé au portail correspondant</h6>
        <hr class="hr"/>
        <div>
          <form action="login" method="post">
            <div class="form-group mb-2">
              <label for="username">
                Nom d'utilisateur
              </label>
              <input class="form-control shadow-sm" id="username" name="username" type="text">
            </div>
            <div class="form-group mb-2">
              <label for="password">
                Mot de passe
              </label>
              <input class="form-control shadow-sm" id="password" name="password" type="password" placeholder="*****">
            </div>
            <hr class="hr"/>
            <div class="row px-3">
              <button class="btn btn-outline-primary btn-sm col-12 shadow" name="action" value="login" type="submit">Se connecter</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
