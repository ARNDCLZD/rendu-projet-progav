<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
    <body class="h-100">
        <div class="container">
            <div>
                <h2>Bonjour ${admin.login}</h2>
            </div>
            <div>
                <h2>Professeurs en attente de validation</h2>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Login</th>
                            <th scope="col">Prénom</th>
                            <th scope="col">Nom</th>
                            <th scope="col">N° de téléphone</th>
                            <th scope="col">Email</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="currentTeacherPending" items="${teachersPending}" >
                            <tr>
                                <td>${currentTeacherPending.login}</td>
                                <td>${currentTeacherPending.firstName}</td>
                                <td>${currentTeacherPending.name}</td>
                                <td>${currentTeacherPending.phone}</td>
                                <td>${currentTeacherPending.email}</td>
                                <td class="d-flex justify-content-evenly">
                                    <button type=button onclick="add('${currentTeacherPending.login}')" class="btn btn-sm btn-success" >+</button>
                                    <button type=button onclick="remove('${currentTeacherPending.login}')" class="btn btn-sm btn-danger" name="action" value="remove">-</button>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <h2>Comptes professeurs</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Login</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Nom</th>
                        <th scope="col">N° de téléphone</th>
                        <th scope="col">Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="teacher" items="${teachers}" >
                        <tr>
                            <td>${teacher.login}</td>
                            <td>${teacher.firstName}</td>
                            <td>${teacher.name}</td>
                            <td>${teacher.phone}</td>
                            <td>${teacher.email}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <h2>Professeurs en attente de validation</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Login</th>
                        <th scope="col">Raison sociale</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="school" items="${schools}" >
                        <tr>
                            <td>${school.login}</td>
                            <td>${school.socialReason}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
<script>
    function add(loginTeacher) {
        $.ajax({
            type: "GET",
            url: "/ProjetProgAv-1.0-SNAPSHOT/admin",
            data: { action: "add", loginTeacher: loginTeacher },
            success: function (response) {
                console.log("allo",response);
            }
        });
    }
    function remove(loginTeacher) {
        $.ajax({
            type: "GET",
            url: "/ProjetProgAv-1.0-SNAPSHOT/admin",
            data: { action: "remove", loginTeacher: loginTeacher },
            success: function (response) {
            }
        });
    }
</script>
