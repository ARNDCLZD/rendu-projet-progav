<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>ALT </title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <link href="css/styles.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container" >
            <h3> <strong> Bonjour ${ school.socialReason } ! </strong></h3>
            <div class="row">
                <form method="post" action="school">

                    <c:choose>
                        <c:when test="${teachersByOffer.size() == 0}">
                            <h3 class="text-danger">Aucun professeur n'a postulé pour cette école</h3>
                        </c:when>
                        <c:otherwise>
                            <h1>Offres d'emplois</h1>
                            <c:forEach items="${teachersByJobOffer}" var="jobOffer" varStatus="loop">
                                <div class="bg-light rounded m-2 p-2 shadow">
                                    <h4>Offre n°${loop.count}</h4>
                                    <div class="row mb-2">
                                        <div class="col-4">
                                            <h4>Specifications :</h4>
                                            <span>${jobOffer.jobOffer.jobSpecification}</span>
                                        </div>
                                        <div class="col-4">
                                            <h4>Required skills :</h4>
                                            <span>${jobOffer.jobOffer.requiredSkills}</span>
                                        </div>
                                        <div class="col-4">
                                            <h4>Contract length :</h4>
                                            <span>${jobOffer.jobOffer.contractLength}</span>
                                        </div>
                                    </div>
                                    <hr class="hr">
                                    <h3>Candidats</h3>
                                    <div class="row">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr class="text-center">
                                                <th>Nom</th>
                                                <th>Prénom</th>
                                                <th>Téléphone</th>
                                                <th>Email</th>
                                                <th>Website</th>
                                                <th>Experiences</th>
                                                <th>Références</th>
                                                <th>Compétences</th>
                                                <th>Titres</th>
                                                <th>Autres infos</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach items="${jobOffer.teachers}" var="teacher">
                                                <tr>
                                                    <td>${teacher.name}</td>
                                                    <td>${teacher.firstName}</td>
                                                    <td>${teacher.phone}</td>
                                                    <td>${teacher.email}</td>
                                                    <td>${teacher.webSite}</td>
                                                    <td>${teacher.experiences}</td>
                                                    <td>${teacher.jobReferences}</td>
                                                    <td>${teacher.competences}</td>
                                                    <td>${teacher.academicTitles}</td>
                                                    <td>${teacher.otherInformations}</td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </c:forEach>
                            <div>
                                <hr class="hr" />
                                <h3>Créer une offre d'emploi</h3>
                                <form>
                                    <input type="hidden" name="username" value="${school.login}">
                                    <div class="form-group">
                                        <label for="jobSpecification" class="form-label">Spécifications</label>
                                        <textarea class="form-control" rows="4" id="jobSpecification" name="jobSpecification"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="requiredSkills" class="form-label">Compétences requises</label>
                                        <textarea class="form-control" rows="4" id="requiredSkills" name="requiredSkills"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="contractLength" class="form-label">Durée du contrat</label>
                                        <input type=text class="form-control" rows="4" id="contractLength" name="contractLength">
                                    </div>
                                    <button class="btn btn-primary mt-2" name="action" value="createJobOffer">Créer l'offre</button>
                                </form>
                            </div>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${teachersByOffer.size() == 0}">
                        <h3 class="text-danger">Aucun professeur n'a postulé pour cette école</h3>
                    </c:if>
                </form>
            </div>
        </div>
    </body>
</html>
