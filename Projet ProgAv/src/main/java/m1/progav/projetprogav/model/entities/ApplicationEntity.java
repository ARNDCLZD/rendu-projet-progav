package m1.progav.projetprogav.model.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "application", schema = "projetprogav", catalog = "")
@NamedQueries({
        @NamedQuery(name = "getApplicationByTeacher", query = "Select e FROM ApplicationEntity e WHERE e.teacherLogin = :login" ),
        @NamedQuery(name = "getApplicationByJobOffer", query = "Select e FROM ApplicationEntity e WHERE e.idOffer = :id" ),
        @NamedQuery(name = "getApplicationByTeacherAndState", query = "Select e FROM ApplicationEntity e WHERE e.teacherLogin = :login AND e.state = :state" ),
})
public class ApplicationEntity {
    @Basic
    @Id
    @Column(name = "TeacherLogin", nullable = false, length = 15)
    private String teacherLogin;
    @Basic
    @Id
    @Column(name = "IdOffer", nullable = false)
    private int idOffer;
    @Basic
    @Column(name = "State", nullable = false, length = 30)
    private String state;

    public String getTeacherLogin() {
        return teacherLogin;
    }

    public void setTeacherLogin(String teacherLogin) {
        this.teacherLogin = teacherLogin;
    }

    public int getIdOffer() {
        return idOffer;
    }

    public void setIdOffer(int idOffer) {
        this.idOffer = idOffer;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApplicationEntity that = (ApplicationEntity) o;

        if (idOffer != that.idOffer) return false;
        if (teacherLogin != null ? !teacherLogin.equals(that.teacherLogin) : that.teacherLogin != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = teacherLogin != null ? teacherLogin.hashCode() : 0;
        result = 31 * result + idOffer;
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }
}
