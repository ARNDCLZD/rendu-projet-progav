package m1.progav.projetprogav.model.entities;

import jakarta.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "getUserByLogin", query = "Select e FROM UserEntity e WHERE e.login = :login" )
})
@Table(name = "user", schema = "projetprogav", catalog = "")
public class UserEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "Login", nullable = false, length = 15)
    private String login;
    @Basic
    @Column(name = "Password", nullable = false, length = 15)
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
