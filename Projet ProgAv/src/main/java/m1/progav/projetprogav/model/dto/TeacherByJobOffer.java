package m1.progav.projetprogav.model.dto;

import m1.progav.projetprogav.model.entities.JobOfferEntity;
import m1.progav.projetprogav.model.entities.TeacherEntity;

import java.util.List;

public class TeacherByJobOffer {
    private JobOfferEntity jobOffer;
    private List<TeacherEntity> teachers;

    public JobOfferEntity getJobOffer() {
        return jobOffer;
    }

    public void setJobOffer(JobOfferEntity jobOffer) {
        this.jobOffer = jobOffer;
    }

    public List<TeacherEntity> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<TeacherEntity> teachers) {
        this.teachers = teachers;
    }
}
