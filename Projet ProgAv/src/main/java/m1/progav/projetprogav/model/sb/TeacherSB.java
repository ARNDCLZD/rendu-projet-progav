package m1.progav.projetprogav.model.sb;

import jakarta.ejb.Stateless;
import jakarta.persistence.*;
import m1.progav.projetprogav.model.dto.Teacher;
import m1.progav.projetprogav.model.entities.ApplicationEntity;
import m1.progav.projetprogav.model.entities.TeacherEntity;

import java.util.ArrayList;
import java.util.List;
@Stateless
public class TeacherSB {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
    private final EntityManager entityManager = entityManagerFactory.createEntityManager();
    private final EntityTransaction transaction = entityManager.getTransaction();

    public TeacherEntity getTeacherByLogin(String login){
        try{
            Query q = entityManager.createNamedQuery("getTeacherByLogin");
            q.setParameter("login", login);
            return (TeacherEntity) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    public List<TeacherEntity> getTeachersByRegistrationState(String registrationState){
        Query q = entityManager.createNamedQuery("getTeachersByRegistrationState");
        q.setParameter("registrationState", registrationState);
        return q.getResultList();
    }

    public List<TeacherEntity> getTeacherByJobOffer(int jobOfferId) {
        Query q = entityManager.createNamedQuery("getApplicationByJobOffer");
        q.setParameter("id", jobOfferId);
        List<ApplicationEntity> applications = (List<ApplicationEntity>) q.getResultList();
        List<TeacherEntity> teachers = new ArrayList<>();
        for (ApplicationEntity application : applications) {
            teachers.add(getTeacherByLogin(application.getTeacherLogin()));
        }
        return teachers;
    }

    public List<TeacherEntity> getAllTeachers(){
        Query q = entityManager.createNamedQuery("getAllTeachers");
        return (List<TeacherEntity>) q.getResultList();
    }

    public boolean createTeacher(Teacher teacher){
        transaction.begin();
        try{
            entityManager.createNativeQuery("INSERT INTO Teacher VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
                    .setParameter(1, teacher.getLogin())
                    .setParameter(2, teacher.getName())
                    .setParameter(3, teacher.getFirstName())
                    .setParameter(4, teacher.getExperiences())
                    .setParameter(5, teacher.getEvaluations())
                    .setParameter(6, teacher.getCompetences())
                    .setParameter(7, teacher.getEmail())
                    .setParameter(8, teacher.getPhone())
                    .setParameter(9, teacher.getWebSite())
                    .setParameter(10, teacher.getAcademicTitles())
                    .setParameter(11, teacher.getOtherInformations())
                    .setParameter(12,teacher.getJobReferences())
                    .setParameter(13,"waiting")
                    .executeUpdate();
            transaction.commit();
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public boolean changeTeacherStatusByLogin(String loginTeacher, String status) {
        try{
            transaction.begin();
            entityManager.createNativeQuery("UPDATE Teacher e SET e.RegistrationState = ? WHERE e.Login=?")
                    .setParameter(1,status)
                    .setParameter(2,loginTeacher)
                    .executeUpdate();
            transaction.commit();
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
