package m1.progav.projetprogav.model.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "joboffer", schema = "projetprogav", catalog = "")
@NamedQueries({
        @NamedQuery(name = "getJobOffersFromSchool", query = "Select e FROM JobOfferEntity e WHERE e.schoolLogin = :login" ),
        @NamedQuery(name = "getAllJobOffers", query = "Select e FROM JobOfferEntity e" ),
        @NamedQuery(name = "getJobOfferById", query = "Select e FROM JobOfferEntity e WHERE e.idOffer=:id" )
})
public class JobOfferEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "IdOffer", nullable = false)
    private int idOffer;
    @Basic
    @Column(name = "SchoolLogin", nullable = false, length = 15)
    private String schoolLogin;
    @Basic
    @Column(name = "JobSpecification", nullable = false, length = -1)
    private String jobSpecification;
    @Basic
    @Column(name = "RequiredSkills", nullable = false, length = -1)
    private String requiredSkills;
    @Basic
    @Column(name = "ContractLength", nullable = false, length = -1)
    private String contractLength;

    public int getIdOffer() {
        return idOffer;
    }

    public void setIdOffer(int idOffer) {
        this.idOffer = idOffer;
    }

    public String getSchoolLogin() {
        return schoolLogin;
    }

    public void setSchoolLogin(String schoolLogin) {
        this.schoolLogin = schoolLogin;
    }

    public String getJobSpecification() {
        return jobSpecification;
    }

    public void setJobSpecification(String jobSpecification) {
        this.jobSpecification = jobSpecification;
    }

    public String getRequiredSkills() {
        return requiredSkills;
    }

    public void setRequiredSkills(String requiredSkills) {
        this.requiredSkills = requiredSkills;
    }

    public String getcontractLength() {
        return contractLength;
    }

    public void setcontractLength(String contractLength) {
        this.contractLength = contractLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JobOfferEntity that = (JobOfferEntity) o;

        if (idOffer != that.idOffer) return false;
        if (schoolLogin != null ? !schoolLogin.equals(that.schoolLogin) : that.schoolLogin != null) return false;
        if (jobSpecification != null ? !jobSpecification.equals(that.jobSpecification) : that.jobSpecification != null)
            return false;
        if (requiredSkills != null ? !requiredSkills.equals(that.requiredSkills) : that.requiredSkills != null)
            return false;
        if (contractLength != null ? !contractLength.equals(that.contractLength) : that.contractLength != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idOffer;
        result = 31 * result + (schoolLogin != null ? schoolLogin.hashCode() : 0);
        result = 31 * result + (jobSpecification != null ? jobSpecification.hashCode() : 0);
        result = 31 * result + (requiredSkills != null ? requiredSkills.hashCode() : 0);
        result = 31 * result + (contractLength != null ? contractLength.hashCode() : 0);
        return result;
    }
}
