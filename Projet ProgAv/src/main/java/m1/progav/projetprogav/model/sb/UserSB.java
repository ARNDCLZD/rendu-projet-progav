package m1.progav.projetprogav.model.sb;

import jakarta.ejb.Stateless;
import jakarta.persistence.*;
import m1.progav.projetprogav.model.entities.UserEntity;

@Stateless
public class UserSB {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
    private final EntityManager entityManager = entityManagerFactory.createEntityManager();
    private EntityTransaction transaction = entityManager.getTransaction();

    public UserEntity getUserByLogin(String login){
        try {
            Query q = entityManager.createNamedQuery("getUserByLogin");
            q.setParameter("login", login);
            return (UserEntity) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    public boolean createUser(String login, String password){
        try {
            transaction.begin();
            entityManager.createNativeQuery("INSERT INTO User VALUES (?,?)")
                    .setParameter(1,login)
                    .setParameter(2,password)
                    .executeUpdate();
            transaction.commit();
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
