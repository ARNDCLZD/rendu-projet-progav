package m1.progav.projetprogav.model.sb;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;
import m1.progav.projetprogav.model.entities.SchoolEntity;

import java.util.List;

@Stateless
public class SchoolSB {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
    private final EntityManager entityManager = entityManagerFactory.createEntityManager();


    public SchoolEntity getSchoolByLogin(String login){
        try {
            Query q = entityManager.createNamedQuery("getSchoolByLogin");
            q.setParameter("login", login);
            return (SchoolEntity) q.getSingleResult();
        }catch(Exception e) {
            return null;
        }
    }
    public List<SchoolEntity> getAllSchools() {
        Query q = entityManager.createNamedQuery("getAllSchools");
        return q.getResultList();
    }
}
