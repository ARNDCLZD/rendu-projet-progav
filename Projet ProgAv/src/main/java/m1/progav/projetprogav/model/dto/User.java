package m1.progav.projetprogav.model.dto;

import m1.progav.projetprogav.utils.UserType;

public class User {
    private String enteredLogin;
    private String enteredPassword;

    private UserType enteredUsertype;
    public String getEnteredLogin() {
        return enteredLogin;
    }

    public void setEnteredLogin(String enteredLogin) {
        this.enteredLogin = enteredLogin;
    }

    public UserType getEnteredUserType() {
        return this.enteredUsertype;
    }

    public void setEnteredUserType(UserType enteredUsertype) {
        this.enteredUsertype = enteredUsertype;
    }

    public void setEnteredUserType(String enteredUsertype) throws Exception {
        switch(enteredUsertype){
            case "school":
                this.enteredUsertype=UserType.School;
                break;
            case "teacher":
                this.enteredUsertype=UserType.Teacher;
                break;
            case "admin":
                this.enteredUsertype=UserType.Admin;
                break;
            default:
                throw new Exception("Type d'utilisateur inconnu");
        }
    }
    public String getEnteredPassword() {
        return enteredPassword;
    }

    public void setEnteredPassword(String enteredPassword) {
        this.enteredPassword = enteredPassword;
    }

}
