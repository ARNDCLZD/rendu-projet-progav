package m1.progav.projetprogav.model.sb;

import jakarta.ejb.Stateless;
import jakarta.persistence.*;
import m1.progav.projetprogav.model.entities.ApplicationEntity;
import m1.progav.projetprogav.model.entities.JobOfferEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Stateless
public class JobOfferSB {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
    private final EntityManager entityManager = entityManagerFactory.createEntityManager();
    private final EntityTransaction transaction = entityManager.getTransaction();

    public boolean createJobOffer(String username, String jobSpecification, String requiredSkills, String contractLength){
        transaction.begin();
        entityManager.createNativeQuery("INSERT INTO Joboffer(SchoolLogin,JobSpecification,RequiredSkills,ContractLength) VALUES (?,?,?,?)")
                .setParameter(1,username)
                .setParameter(2,jobSpecification)
                .setParameter(3,requiredSkills)
                .setParameter(4,contractLength)
                .executeUpdate();
        transaction.commit();
        return true;
    }
    public List<JobOfferEntity> getJobOffersFromSchool(String schoolLogin){
        Query q = entityManager.createNamedQuery("getJobOffersFromSchool");
        q.setParameter("login", schoolLogin);
        return q.getResultList();

    }
    public List<JobOfferEntity> getAllJobOffers() {
        Query q = entityManager.createNamedQuery("getAllJobOffers");
        return q.getResultList();
    }

    public List<JobOfferEntity> getJobOffersIfApplicationExists(String teacherLogin) {
        Query q = entityManager.createNamedQuery("getApplicationByTeacher");
        q.setParameter("login", teacherLogin);
        List<ApplicationEntity> applications = q.getResultList();
        List <JobOfferEntity> jobOffers = new ArrayList<>();

        for (int i = 0; i< applications.size();i++){
            ApplicationEntity application = applications.get(i);
            q = entityManager.createNamedQuery("getJobOfferById");
            q.setParameter("id", application.getIdOffer());
            jobOffers.add((JobOfferEntity) q.getSingleResult());
        }
        return jobOffers;
    }

    public List<JobOfferEntity> getJobOffersIfApplicationDoesntExist(String teacherLogin) {
        List<JobOfferEntity> jobOffers = getJobOffersIfApplicationExists(teacherLogin);

        List<JobOfferEntity> allJobOffers = getAllJobOffers();
        Set<JobOfferEntity> set1 = new HashSet<>(jobOffers);
        Set<JobOfferEntity> set2 = new HashSet<>(allJobOffers);
        set2.removeAll(set1);
        List<JobOfferEntity> result = new ArrayList<>(set2);
        return result;
    }

    public List<JobOfferEntity> getJobApplicationByTeacherAndState(String teacherLogin, String state){
        Query q = entityManager.createNamedQuery("getApplicationByTeacherAndState");
        q.setParameter("login", teacherLogin);
        q.setParameter("state", state);
        List<ApplicationEntity> applicationsByState = q.getResultList();
        List <JobOfferEntity> jobOffers = new ArrayList<>();

        for (int i = 0; i< applicationsByState.size();i++){
            ApplicationEntity application = applicationsByState.get(i);
            q = entityManager.createNamedQuery("getJobOfferById");
            q.setParameter("id", application.getIdOffer());
            jobOffers.add((JobOfferEntity) q.getSingleResult());
        }
        return jobOffers;
    }
}
