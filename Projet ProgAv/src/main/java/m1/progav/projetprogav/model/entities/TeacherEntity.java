package m1.progav.projetprogav.model.entities;

import jakarta.persistence.*;
import m1.progav.projetprogav.model.dto.Teacher;

@Entity
@Table(name = "teacher", schema = "projetprogav", catalog = "")
@NamedQueries({
        @NamedQuery(name = "getTeacherByLogin", query = "Select e FROM TeacherEntity e WHERE e.login = :login" ),
        @NamedQuery(name = "getTeachersByRegistrationState", query = "Select e FROM TeacherEntity e WHERE e.registrationState = :registrationState "),
        @NamedQuery(name = "getAllTeachers", query = "Select e FROM TeacherEntity e"),
})
public class TeacherEntity {
    @Basic
    @Id
    @Column(name = "Login", nullable = false, length = 15)
    private String login;
    @Basic
    @Column(name = "Name", nullable = false, length = 15)
    private String name;
    @Basic
    @Column(name = "FirstName", nullable = false, length = 15)
    private String firstName;
    @Basic
    @Column(name = "Experiences", nullable = false, length = -1)
    private String experiences;
    @Basic
    @Column(name = "Competences", nullable = false, length = -1)
    private String competences;
    @Basic
    @Column(name = "Evaluations", nullable = false, length = -1)
    private String evaluations;
    @Basic
    @Column(name = "Email", nullable = false, length = 30)
    private String email;
    @Basic
    @Column(name = "Phone", nullable = false, length = 10)
    private String phone;
    @Basic
    @Column(name = "webSite", nullable = false, length = -1)
    private String webSite;
    @Basic
    @Column(name = "AcademicTitles", nullable = false, length = -1)
    private String academicTitles;
    @Basic
    @Column(name = "OtherInformations", nullable = false, length = -1)
    private String otherInformations;
    @Basic
    @Column(name = "JobReferences", nullable = false, length = -1)
    private String jobReferences;
    @Basic
    @Column(name = "RegistrationState", nullable = false, length = 30)
    private String registrationState;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getExperiences() {
        return experiences;
    }

    public void setExperiences(String experiences) {
        this.experiences = experiences;
    }

    public String getCompetences() {
        return competences;
    }

    public void setCompetences(String competences) {
        this.competences = competences;
    }

    public String getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(String evaluations) {
        this.evaluations = evaluations;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getAcademicTitles() {
        return academicTitles;
    }

    public void setAcademicTitles(String academicTitles) {
        this.academicTitles = academicTitles;
    }

    public String getOtherInformations() {
        return otherInformations;
    }

    public void setOtherInformations(String otherInformations) {
        this.otherInformations = otherInformations;
    }

    public String getJobReferences() {
        return jobReferences;
    }

    public void setJobReferences(String jobReferences) {
        this.jobReferences = jobReferences;
    }

    public String getRegistrationState() {
        return registrationState;
    }

    public void setRegistrationState(String inscriptionState) {
        this.registrationState = inscriptionState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TeacherEntity that = (TeacherEntity) o;

        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (experiences != null ? !experiences.equals(that.experiences) : that.experiences != null) return false;
        if (competences != null ? !competences.equals(that.competences) : that.competences != null) return false;
        if (evaluations != null ? !evaluations.equals(that.evaluations) : that.evaluations != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (webSite != null ? !webSite.equals(that.webSite) : that.webSite != null) return false;
        if (academicTitles != null ? !academicTitles.equals(that.academicTitles) : that.academicTitles != null)
            return false;
        if (otherInformations != null ? !otherInformations.equals(that.otherInformations) : that.otherInformations != null)
            return false;
        if (jobReferences != null ? !jobReferences.equals(that.jobReferences) : that.jobReferences != null)
            return false;
        if (registrationState != null ? !registrationState.equals(that.registrationState) : that.registrationState != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (experiences != null ? experiences.hashCode() : 0);
        result = 31 * result + (competences != null ? competences.hashCode() : 0);
        result = 31 * result + (evaluations != null ? evaluations.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (webSite != null ? webSite.hashCode() : 0);
        result = 31 * result + (academicTitles != null ? academicTitles.hashCode() : 0);
        result = 31 * result + (otherInformations != null ? otherInformations.hashCode() : 0);
        result = 31 * result + (jobReferences != null ? jobReferences.hashCode() : 0);
        result = 31 * result + (registrationState != null ? registrationState.hashCode() : 0);
        return result;
    }

    public Teacher convertToTeacher() {
        Teacher teacher = new Teacher();

        teacher.setLogin(this.getLogin());
        teacher.setName(this.getName());
        teacher.setFirstName(this.getFirstName());
        teacher.setExperiences(this.getExperiences());
        teacher.setCompetences(this.getCompetences());
        teacher.setEvaluations(this.getEvaluations());
        teacher.setEmail(this.getEmail());
        teacher.setPhone(this.getPhone());
        teacher.setWebSite(this.getWebSite());
        teacher.setAcademicTitles(this.getAcademicTitles());
        teacher.setOtherInformations(this.getOtherInformations());
        teacher.setJobReferences(this.getJobReferences());

        return teacher;
    }
}