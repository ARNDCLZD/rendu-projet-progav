package m1.progav.projetprogav.model.sb;

import jakarta.ejb.Stateless;
import jakarta.persistence.*;
import m1.progav.projetprogav.model.entities.AdminEntity;

@Stateless
public class AdminSB {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
    private final EntityManager entityManager = entityManagerFactory.createEntityManager();
    private final EntityTransaction transaction = entityManager.getTransaction();
    public AdminEntity getAdminByLogin(String login){
        Query q = entityManager.createNamedQuery("getAdminByLogin");
        q.setParameter("login",login);
        return ((AdminEntity) q.getSingleResult());
    }

}
