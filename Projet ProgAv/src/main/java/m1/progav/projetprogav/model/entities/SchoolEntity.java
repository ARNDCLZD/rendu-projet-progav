package m1.progav.projetprogav.model.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "school", schema = "projetprogav", catalog = "")

@NamedQueries({
        @NamedQuery(name = "getSchoolByLogin", query = "Select e FROM SchoolEntity e WHERE e.login = :login" ),
        @NamedQuery(name = "getAllSchools", query = "Select e FROM SchoolEntity e" )
})
public class SchoolEntity {
    @Basic
    @Id
    @Column(name = "Login", nullable = false, length = 15)
    private String login;
    @Basic
    @Column(name = "SocialReason", nullable = false, length = 15)
    private String socialReason;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSocialReason() {
        return socialReason;
    }

    public void setSocialReason(String socialReason) {
        this.socialReason = socialReason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SchoolEntity that = (SchoolEntity) o;

        if (login != null ? !login.equals(that.login) : that.login != null) return false;
        if (socialReason != null ? !socialReason.equals(that.socialReason) : that.socialReason != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = login != null ? login.hashCode() : 0;
        result = 31 * result + (socialReason != null ? socialReason.hashCode() : 0);
        return result;
    }
}
