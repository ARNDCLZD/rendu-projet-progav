package m1.progav.projetprogav.model.sb;

import jakarta.ejb.Stateless;
import jakarta.persistence.*;
import m1.progav.projetprogav.model.entities.ApplicationEntity;

import java.util.List;
@Stateless
public class ApplicationSB {
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
    private final EntityManager entityManager = entityManagerFactory.createEntityManager();
    private final EntityTransaction transaction = entityManager.getTransaction();
    public List<ApplicationEntity> getApplicationsByTeacher(String teacherLogin){
        Query q = entityManager.createNamedQuery("getApplicationByTeacher");
        q.setParameter("login", teacherLogin);
        return q.getResultList();
    }

    public List<ApplicationEntity> getApplicationsByJobOffer(int id){
        Query q = entityManager.createNamedQuery("getApplicationByJobOffer");
        q.setParameter("id", id);
        return q.getResultList();
    }

    public void createApplication(String teacherLogin, int idOffer) {
        transaction.begin();
        entityManager.createNativeQuery("INSERT INTO Application(teacherLogin, idOffer, state) VALUES (?, ?, ?)")
                .setParameter(1, teacherLogin)
                .setParameter(2, idOffer)
                .setParameter(3, "waiting")
                .executeUpdate();
        transaction.commit();

    }

    public void deleteApplication(String teacherLogin, int idOffer) {
        transaction.begin();
        entityManager.createNativeQuery("DELETE FROM Application WHERE idOffer = ? AND teacherLogin = ?")
                .setParameter(2, teacherLogin)
                .setParameter(1, idOffer)
                .executeUpdate();
        transaction.commit();

    }
}

