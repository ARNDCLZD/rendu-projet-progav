package m1.progav.projetprogav.model.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "admin", schema = "projetprogav", catalog = "")
@NamedQueries({
        @NamedQuery(name = "getAdminByLogin", query = "Select admin FROM AdminEntity admin WHERE admin.login = :login" ),
})
public class AdminEntity {
    @Basic
    @Id
    @Column(name = "Login", nullable = false, length = 15)
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdminEntity that = (AdminEntity) o;

        if (login != null ? !login.equals(that.login) : that.login != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return login != null ? login.hashCode() : 0;
    }
}
