package m1.progav.projetprogav.model.dto;

public class Teacher {
    private String login;
    private String name;
    private String firstName;
    private String experiences;
    private String evaluations;
    private String competences;
    private String email;
    private String webSite;
    private String phone;
    private String academicTitles;
    private String jobReferences;
    private String otherInformations;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getExperiences() {
        return experiences;
    }

    public void setExperiences(String experiences) {
        this.experiences = experiences;
    }

    public String getCompetences() {
        return competences;
    }

    public void setCompetences(String competences) {
        this.competences = competences;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAcademicTitles() {
        return academicTitles;
    }

    public void setAcademicTitles(String academicTitles) {
        this.academicTitles = academicTitles;
    }

    public String getJobReferences() {
        return jobReferences;
    }

    public void setJobReferences(String jobReferences) {
        this.jobReferences = jobReferences;
    }

    public String getOtherInformations() {
        return otherInformations;
    }

    public void setOtherInformations(String otherInformations) {
        this.otherInformations = otherInformations;
    }

    public String getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(String evaluations) {
        this.evaluations = evaluations;
    }
}
