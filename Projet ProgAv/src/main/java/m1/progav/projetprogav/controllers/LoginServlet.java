package m1.progav.projetprogav.controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import m1.progav.projetprogav.model.dto.TeacherByJobOffer;
import m1.progav.projetprogav.model.entities.*;
import m1.progav.projetprogav.model.sb.*;
import m1.progav.projetprogav.model.dto.Teacher;
import m1.progav.projetprogav.model.dto.User;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

public class LoginServlet extends HttpServlet {
    private UserSB userSB = new UserSB();
    private TeacherSB teacherSB = new TeacherSB();
    private SchoolSB schoolSB = new SchoolSB();
    private JobOfferSB jobOfferSB = new JobOfferSB();
    private AdminSB adminSB = new AdminSB();
    private ApplicationSB applicationSB = new ApplicationSB();
    private User user = new User();

    public void processRequest (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getParameter("action") == null)
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        else{
            String login = request.getParameter("username");
            String password = request.getParameter("password");
            user.setEnteredLogin(login);
            user.setEnteredPassword(password);
            boolean userExists = connectionTryResult(user);
            switch(request.getParameter("action")){
                case "login":
                    if (userExists){
                        handleConnexion(user, request, response);
                    }
                    else{
                        System.out.println("connexion marche pas " + login);
                    }
                    break;
                case "register":
                    if (!userExists){
                        userSB.createUser(login,password);
                        Teacher teacher = new Teacher();
                        teacher.setLogin(request.getParameter("username"));
                        teacher.setFirstName(request.getParameter("firstName"));
                        teacher.setName(request.getParameter("name"));
                        teacher.setEmail(request.getParameter("email"));
                        teacher.setEvaluations(request.getParameter("evaluations"));
                        teacher.setCompetences(request.getParameter("skills"));
                        teacher.setExperiences(request.getParameter("experiences"));
                        teacher.setPhone(request.getParameter("phone"));
                        teacher.setAcademicTitles(request.getParameter("academicTitles"));
                        teacher.setWebSite(request.getParameter("website"));
                        teacher.setOtherInformations(request.getParameter("otherInformations"));
                        teacher.setJobReferences(request.getParameter("jobReferences"));
                        handleRegistration(teacher, user, request, response);
                    }
                    break;
            }
        }
    }
    public boolean connectionTryResult(User user) {
        UserEntity existingUser = userSB.getUserByLogin(user.getEnteredLogin());
        if (existingUser == null)
            return false;
        else {
            return Objects.equals(existingUser.getPassword(), user.getEnteredPassword());
        }
    }


    public String getUserType(User user){
        String login = user.getEnteredLogin();
        if (teacherSB.getTeacherByLogin(login) != null)
            return "teacher";
        else if (schoolSB.getSchoolByLogin(login) != null)
            return "school";
        else if(adminSB.getAdminByLogin(login) != null)
            return "admin";
        else return null;
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    public void handleConnexion(User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userType = getUserType(user);
        List<JobOfferEntity> jobOffers;
        switch (userType){
            case ("teacher"):
                TeacherEntity teacher = teacherSB.getTeacherByLogin(request.getParameter("username"));
                List<JobOfferEntity> jobOffersNotApplied= jobOfferSB.getJobOffersIfApplicationDoesntExist(request.getParameter("loginField"));
                List<JobOfferEntity> jobOffersAppliedWaiting = jobOfferSB.getJobApplicationByTeacherAndState(request.getParameter("loginField"), "waiting");
                List<JobOfferEntity> jobOffersAppliedAccepted = jobOfferSB.getJobApplicationByTeacherAndState(request.getParameter("loginField"), "accepted");
                List<JobOfferEntity> jobOffersAppliedRefused = jobOfferSB.getJobApplicationByTeacherAndState(request.getParameter("loginField"), "refused");
                request.getSession().setAttribute("user", teacher);
                request.setAttribute("teacher", teacher);
                request.setAttribute("jobOffersNotApplied", jobOffersNotApplied);
                request.setAttribute("jobOffersApplicationWaiting", jobOffersAppliedWaiting);
                request.setAttribute("jobOffersApplicationAccepted", jobOffersAppliedAccepted);
                request.setAttribute("jobOffersApplicationRefused", jobOffersAppliedRefused);
                break;
            case ("school"):
                SchoolEntity school = schoolSB.getSchoolByLogin(request.getParameter("username"));
                jobOffers = jobOfferSB.getJobOffersFromSchool(request.getParameter("username"));
                List<TeacherByJobOffer> teachersByJobOffer = new ArrayList<>();
                int jobOfferId;
                for (JobOfferEntity jobOffer: jobOffers){
                    TeacherByJobOffer teacherByOneJobOffer = new TeacherByJobOffer();
                    teacherByOneJobOffer.setJobOffer(jobOffer);
                    teacherByOneJobOffer.setTeachers(teacherSB.getTeacherByJobOffer(jobOffer.getIdOffer()));
                    teachersByJobOffer.add(teacherByOneJobOffer);
                }
                request.getSession().setAttribute("user",school);
                request.setAttribute("teachersByJobOffer", teachersByJobOffer);
                request.setAttribute("jobOffers", jobOffers);
                request.setAttribute("school", school);
                break;
            case ("admin"):
                AdminEntity admin = adminSB.getAdminByLogin(request.getParameter("username"));
                List<TeacherEntity> teachersWithRegistrationPending = teacherSB.getTeachersByRegistrationState("waiting");
                List<Teacher> teachersPending = new ArrayList<>();
                for (TeacherEntity teacherWithRegistrationPending : teachersWithRegistrationPending){
                    teachersPending.add(teacherWithRegistrationPending.convertToTeacher());
                }
                List<TeacherEntity> teachers = teacherSB.getAllTeachers();
                List<SchoolEntity> schools = schoolSB.getAllSchools();
                request.getSession().setAttribute("user",admin);
                request.setAttribute("admin",admin);
                request.setAttribute("teachersPending",teachersPending);
                request.setAttribute("teachers", teachers);
                request.setAttribute("schools", schools);
                break;
        }
        request.getRequestDispatcher("/" + userType + ".jsp").forward(request, response);

    }
    public void handleRegistration(Teacher teacher, User user, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        teacherSB.createTeacher(teacher);
        handleConnexion(user,request,response);
    }
}

