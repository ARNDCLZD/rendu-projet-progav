package m1.progav.projetprogav.controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import m1.progav.projetprogav.model.dto.Teacher;
import m1.progav.projetprogav.model.entities.TeacherEntity;
import m1.progav.projetprogav.model.sb.TeacherSB;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AdminController extends HttpServlet {

    private TeacherSB teacherSB = new TeacherSB();

    protected <ObjectMapper> void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        switch(action){
            case "add":
                teacherSB.changeTeacherStatusByLogin(request.getParameter("loginTeacher"),"accepted");
                List<TeacherEntity> teachersPending = teacherSB.getTeachersByRegistrationState("waiting");
                String jsonResponse = "{ \"result\": \"success\", \"teachersPending\": [";
                for (int i = 0; i < teachersPending.size(); i++) {
                    jsonResponse += "{ \"login\": " + teachersPending.get(i).getLogin() + ", \"firstName\": \"" + teachersPending.get(i).getFirstName() + "\", \"name\": \"" + teachersPending.get(i).getName() + "\", \"email\": \"" + teachersPending.get(i).getEmail() + "\" }";
                    if (i < teachersPending.size() - 1) {
                        jsonResponse += ", ";
                    }
                }
                jsonResponse += "] }";
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                try (PrintWriter out = response.getWriter()) {
                    out.print(jsonResponse);
                    out.flush();
                }
                break;
            case "remove":
                teacherSB.changeTeacherStatusByLogin(request.getParameter("loginTeacher"),"refused");
                break;
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

}
