package m1.progav.projetprogav.controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import m1.progav.projetprogav.model.entities.JobOfferEntity;
import m1.progav.projetprogav.model.entities.TeacherEntity;
import m1.progav.projetprogav.model.sb.ApplicationSB;
import m1.progav.projetprogav.model.sb.JobOfferSB;
import m1.progav.projetprogav.model.sb.TeacherSB;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

public class TeacherController extends HttpServlet {
    TeacherSB teacherSB = new TeacherSB();
    ApplicationSB applicationSB = new ApplicationSB();
    JobOfferSB jobOfferSB = new JobOfferSB();
    TeacherEntity teacher;

    protected <ObjectMapper> void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (Objects.equals(request.getParameter("action"), "apply")) {
            applicationSB.createApplication(request.getParameter("teacherLogin"), Integer.parseInt(request.getParameter("idOffer")));
        } else if (Objects.equals(request.getParameter("action"), "disapply")) {
            applicationSB.deleteApplication(request.getParameter("teacherLogin"), Integer.parseInt(request.getParameter("idOffer")));
        } else if(Objects.equals(request.getParameter("action"), "disconnect")){
            request.getSession().invalidate();
            request.getRequestDispatcher("/login").forward(request,response);
        }
        List<JobOfferEntity> jobOffersNotApplied = jobOfferSB.getJobOffersIfApplicationDoesntExist(request.getParameter("teacherLogin"));
        List<JobOfferEntity> jobOffersApplied = jobOfferSB.getJobApplicationByTeacherAndState(request.getParameter("teacherLogin"), "waiting");
        String jsonResponse = "{ \"result\": \"success\", \"jobOffersNotApplied\": [";
        for (int i = 0; i < jobOffersNotApplied.size(); i++) {
            jsonResponse += "{ \"idOffer\": " + jobOffersNotApplied.get(i).getIdOffer() + ", \"jobSpecification\": \"" + jobOffersNotApplied.get(i).getJobSpecification() + "\", \"requiredSkills\": \"" + jobOffersNotApplied.get(i).getRequiredSkills() + "\", \"contractLength\": \"" + jobOffersNotApplied.get(i).getcontractLength() + "\" }";
            if (i < jobOffersNotApplied.size() - 1) {
                jsonResponse += ", ";
            }
        }
        jsonResponse += "], \"jobApplicationsWaiting\": [";
        for (int i = 0; i < jobOffersApplied.size(); i++) {
            jsonResponse += "{ \"idOffer\": " + jobOffersApplied.get(i).getIdOffer() + ", \"jobSpecification\": \"" + jobOffersApplied.get(i).getJobSpecification() + "\", \"requiredSkills\": \"" + jobOffersApplied.get(i).getRequiredSkills() + "\", \"contractLength\": \"" + jobOffersApplied.get(i).getcontractLength() + "\" }";
            if (i < jobOffersApplied.size() - 1) {
                jsonResponse += ", ";
            }
        }
        jsonResponse += "] }";
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.print(jsonResponse);
            out.flush();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

}

