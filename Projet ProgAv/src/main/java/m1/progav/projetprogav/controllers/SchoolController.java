package m1.progav.projetprogav.controllers;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import m1.progav.projetprogav.model.dto.TeacherByJobOffer;
import m1.progav.projetprogav.model.entities.JobOfferEntity;
import m1.progav.projetprogav.model.entities.SchoolEntity;
import m1.progav.projetprogav.model.sb.JobOfferSB;
import m1.progav.projetprogav.model.sb.SchoolSB;
import m1.progav.projetprogav.model.sb.TeacherSB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SchoolController extends HttpServlet {

    private JobOfferSB jobOfferSB = new JobOfferSB();
    private SchoolSB schoolSB = new SchoolSB();
    private TeacherSB teacherSB = new TeacherSB();

    protected <ObjectMapper> void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        switch(action){
            case "createJobOffer":
                jobOfferSB.createJobOffer(request.getParameter("username"), request.getParameter("jobSpecification"),request.getParameter("requiredSkills"),request.getParameter("contractLength"));
                SchoolEntity school = schoolSB.getSchoolByLogin(request.getParameter("username"));
                List<JobOfferEntity> jobOffers = jobOfferSB.getJobOffersFromSchool(request.getParameter("username"));
                List<TeacherByJobOffer> teachersByJobOffer = new ArrayList<>();
                for (JobOfferEntity jobOffer: jobOffers){
                    TeacherByJobOffer teacherByOneJobOffer = new TeacherByJobOffer();
                    teacherByOneJobOffer.setJobOffer(jobOffer);
                    teacherByOneJobOffer.setTeachers(teacherSB.getTeacherByJobOffer(jobOffer.getIdOffer()));
                    teachersByJobOffer.add(teacherByOneJobOffer);
                }
                request.getSession().setAttribute("user",school);
                request.setAttribute("teachersByJobOffer", teachersByJobOffer);
                request.setAttribute("jobOffers", jobOffers);
                request.setAttribute("school", school);
                request.getRequestDispatcher("/school.jsp").forward(request,response);
                break;
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }

    public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        processRequest(request, response);
    }
}
