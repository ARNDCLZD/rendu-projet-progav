-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 10 nov. 2023 à 22:36
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `projetprogav`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `Login` varchar(15) NOT NULL,
  KEY `Login` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`Login`) VALUES
('admin');

-- --------------------------------------------------------

--
-- Structure de la table `application`
--

DROP TABLE IF EXISTS `application`;
CREATE TABLE IF NOT EXISTS `application` (
  `TeacherLogin` varchar(15) NOT NULL,
  `IdOffer` int(11) NOT NULL,
  `State` varchar(30) NOT NULL,
  KEY `idOffer` (`IdOffer`),
  KEY `teacherLogin` (`TeacherLogin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `application`
--

INSERT INTO `application` (`TeacherLogin`, `IdOffer`, `State`) VALUES
('teacher', 3, 'waiting');

-- --------------------------------------------------------

--
-- Structure de la table `joboffer`
--

DROP TABLE IF EXISTS `joboffer`;
CREATE TABLE IF NOT EXISTS `joboffer` (
  `IdOffer` int(11) NOT NULL AUTO_INCREMENT,
  `SchoolLogin` varchar(15) NOT NULL,
  `JobSpecification` text NOT NULL,
  `RequiredSkills` text NOT NULL,
  `ContractLength` text NOT NULL,
  PRIMARY KEY (`IdOffer`),
  KEY `SchollLogin` (`SchoolLogin`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `joboffer`
--

INSERT INTO `joboffer` (`IdOffer`, `SchoolLogin`, `JobSpecification`, `RequiredSkills`, `ContractLength`) VALUES
(3, 'school', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit'),
(4, 'school', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit');

-- --------------------------------------------------------

--
-- Structure de la table `school`
--

DROP TABLE IF EXISTS `school`;
CREATE TABLE IF NOT EXISTS `school` (
  `Login` varchar(15) NOT NULL,
  `SocialReason` varchar(15) NOT NULL,
  KEY `Login` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `school`
--

INSERT INTO `school` (`Login`, `SocialReason`) VALUES
('school', 'EFREI');

-- --------------------------------------------------------

--
-- Structure de la table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE IF NOT EXISTS `teacher` (
  `Login` varchar(15) NOT NULL,
  `Name` varchar(15) NOT NULL,
  `FirstName` varchar(15) NOT NULL,
  `Experiences` text NOT NULL,
  `Competences` text NOT NULL,
  `Evaluations` text NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Phone` varchar(10) NOT NULL,
  `webSite` text NOT NULL,
  `AcademicTitles` text NOT NULL,
  `OtherInformations` text NOT NULL,
  `JobReferences` text NOT NULL,
  `RegistrationState` varchar(30) NOT NULL,
  KEY `Login` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `teacher`
--

INSERT INTO `teacher` (`Login`, `Name`, `FirstName`, `Experiences`, `Competences`, `Evaluations`, `Email`, `Phone`, `webSite`, `AcademicTitles`, `OtherInformations`, `JobReferences`, `RegistrationState`) VALUES
('teacher', 'El Mestari', 'Bilel', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'bilel.elmestari@gmail.com', '0657754732', 'https://bilel.com', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit', 'accepted');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `Login` varchar(15) NOT NULL,
  `Password` varchar(15) NOT NULL,
  PRIMARY KEY (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`Login`, `Password`) VALUES
('admin', 'admin'),
('school', 'school'),
('teacher', 'teacher');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `user` (`Login`);

--
-- Contraintes pour la table `application`
--
ALTER TABLE `application`
  ADD CONSTRAINT `application_ibfk_1` FOREIGN KEY (`IdOffer`) REFERENCES `joboffer` (`IdOffer`),
  ADD CONSTRAINT `application_ibfk_2` FOREIGN KEY (`TeacherLogin`) REFERENCES `teacher` (`Login`);

--
-- Contraintes pour la table `joboffer`
--
ALTER TABLE `joboffer`
  ADD CONSTRAINT `joboffer_ibfk_1` FOREIGN KEY (`SchoolLogin`) REFERENCES `school` (`Login`);

--
-- Contraintes pour la table `school`
--
ALTER TABLE `school`
  ADD CONSTRAINT `school_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `user` (`Login`);

--
-- Contraintes pour la table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `user` (`Login`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
